#include <ESP32Servo.h>
#include <BluetoothSerial.h>
BluetoothSerial SerialBT;
byte BTData;

// Sensors
const int sensorPin1 = 19;  // IR sensor left
const int sensorPin2 = 15; // IR sensor right
// variable for storing status
int State1 = 0;
int State2 = 0;
int lastState1 = 0;
int lastState2 = 0;
int pulse1 = 0;
int pulse2 = 0;
int pulse_dif = 0;
int pulse_dif_last = 0;
// Servo
int pos = 84;    // variable to store the servo position
int servoPin = 18;
// Motor
int motor1Pin1 = 27; //rpwm
int motor1Pin2 = 26; //lpwm
int enable1Pin = 4; //enPWM
// Setting PWM properties
const int freq = 30000;
const int pwmChannel = 4;
const int resolution = 8;
int dutyCycle = 200;

Servo myservo;
int dist = 150;
int nuvaz;
int stopFlag = 0;

//PID constants
double kp = 1;
double ki = 0;
double kd = 0.05;

unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
double input, output, setPoint = 0;
double cumError, rateError;

void setup() {
  Serial.begin(115200);
  SerialBT.begin();

  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(enable1Pin, OUTPUT);
  pinMode(sensorPin1, INPUT);
  pinMode(sensorPin2, INPUT);

  // configure LED PWM functionalitites
  ledcSetup(pwmChannel, freq, resolution);
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(enable1Pin, pwmChannel);
  ledcWrite(pwmChannel, dutyCycle);

  myservo.setPeriodHertz(50);    // standard 50 hz servo
  myservo.attach(servoPin, 1000, 2000); // attaches the servo on pin 18 to the servo object

  myservo.write(pos);

  lastState1 = digitalRead(sensorPin1);
  lastState2 = digitalRead(sensorPin2);
  if (lastState1 == 1) {
    lastState1 = 1;
  }
  if (lastState2 == 1) {
    lastState2 = 1;
  }

}

void loop() {

  if (SerialBT.available())
  {
    BTData = SerialBT.read();
    Serial.write(BTData);
  }
  // read the state of the sensors value
  State1 = digitalRead(sensorPin1);
  State2 = digitalRead(sensorPin2);

  if (State1 != lastState1) {
    pulse1++;
    lastState1 = State1;
  }
  if (State2 != lastState2) {
    pulse2++;
    lastState2 = State2;
    /*Serial.print(pulse_dif);
    Serial.print(" PID: ");
    Serial.println(output);
    SerialBT.print(pulse_dif);
    SerialBT.print("PID: ");
    SerialBT.println(output);*/
    //SerialBT.println(nuvaz);
    //SerialBT.println(pulse1 - pulse2);
    //Serial.println(pulse1 - pulse2);
    //Serial.println(computePID(pulse1 - pulse2));
  }
  pulse_dif = pulse1 - pulse2;
  nuvaz = (pulse1 + pulse2) / 2;
  output = computePID(pulse_dif);
  Serial.println(output);

  myservo.write(pos - constrain(output, -5, 5));
  pulse_dif_last = pulse_dif;
  if (pulse_dif != pulse_dif_last) {
    /*SerialBT.println("Skirtumas: ");
      SerialBT.print(pulse_dif);
      SerialBT.print("PID: ");
      SerialBT.println(output);*/
  }

  if (dist <= nuvaz) {
    MotorStop();
  }
  else if (BTData == '1') {
    MotorForward();
    BTData = SerialBT.read();
    Serial.write(BTData);
  }
}

void MotorForward() {
  SerialBT.println("Moving Forward");
  digitalWrite(motor1Pin1, LOW);
  digitalWrite(motor1Pin2, HIGH);
}

void MotorStop() {
  if (stopFlag == 0) {
    SerialBT.println("Motor stopped");
    SerialBT.println(nuvaz - dist );
    stopFlag = 1;
  }
  digitalWrite(motor1Pin1, LOW);
  digitalWrite(motor1Pin2, LOW);
}

double computePID(double inp) {
  currentTime = millis();                //get current time
  elapsedTime = (double)(currentTime - previousTime);        //compute time elapsed from previous computation
  Serial.print(elapsedTime);
  int Setpoint = 0;
  error = Setpoint - inp;                                // determine error
  Serial.print(" ");
  Serial.print(error);
  Serial.print(" ");
  cumError += error * elapsedTime;                // compute integral
  Serial.print(cumError);
  Serial.print(" ");
  rateError = (error - lastError) / elapsedTime; // compute derivative
  Serial.print(rateError);
  Serial.print(" ");
  double out = kp * error + ki * cumError + kd * rateError;          //PID output
  Serial.println(out);
  lastError = error;                                //remember current error
  previousTime = currentTime;                        //remember current time

  return out;                                        //have function return the PID output
}
