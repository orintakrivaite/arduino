##Description
Remote control made with ESP32, MQTT server and Pimatic. Config.txt is a Pimatic file that includes all the rules applied. 

![image](Homeduino_project/schematics_ESP32.PNG)
