#include <SerialCommand.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "DHTesp.h"

#define DHTpin 32 
DHTesp dht;

// Replace the next variables with your SSID/Password combination
const char* ssid = "OK";
const char* password = "mandarinaijega";

const char* mqtt_server = "domain.com";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
unsigned long past;
const int RELAY_1 = 22;
const int RELAY_2 = 25;
const int RELAY_3 = 26;
const int RELAY_4 = 27;

const int PushButton = 15;
const int PushButton_2 = 19;
const int PushButton_3 = 5; //nenaudot
const int PushButton_4 = 18;

bool RState_1 = 0;
bool RState_2 = 0;
bool RState_3 = 0;
bool RState_4 = 0;

bool LastState = 0;
bool LastState_2 = 0;
bool LastState_3 = 0;
bool LastState_4 = 0;

char OnString[8] = {'O', 'N'};
char OffString[8] = {'O', 'F', 'F'};

float temperature = 0;
float humidity = 0;

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  pinMode(RELAY_1, OUTPUT);
  pinMode(RELAY_2, OUTPUT);
  pinMode(RELAY_3, OUTPUT);
  pinMode(RELAY_4, OUTPUT);
  pinMode(PushButton, INPUT_PULLUP);
  pinMode(PushButton_2, INPUT_PULLUP);
  pinMode(PushButton_3, INPUT_PULLUP);
  pinMode(PushButton_4, INPUT_PULLUP);
  dht.setup(DHTpin, DHTesp::DHT11);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;

  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("homeduino")) {
      Serial.println("connected");
      // Subscribe
      //client.subscribe("esp32/output");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  bool Push_button_state = digitalRead(PushButton);
  bool Push_button_state_2 = digitalRead(PushButton_2);
  bool Push_button_state_3 = digitalRead(PushButton_3);
  bool Push_button_state_4 = digitalRead(PushButton_4);
  //First button
  if (Push_button_state != LastState) {
    if (Push_button_state == LOW) {
      RState_1 = !RState_1;
      if (RState_1 == HIGH) {
        digitalWrite(RELAY_1, HIGH);
        client.publish("esp32", "ON1");
        Serial.println("ciaturetubuton");
      }
      if (RState_1 == LOW) {
        digitalWrite(RELAY_1, LOW);
        client.publish("esp32", "OFF1");
        Serial.println("ciaturetubutoff");
      }
      Serial.println("Pasikeite_1");
    }
    delayMicroseconds(15);
  }
  LastState = Push_button_state;

  //Second button
  if (Push_button_state_2 != LastState_2) {
    if (Push_button_state_2 == LOW) {
      RState_2 = !RState_2;
      if (RState_2 == HIGH) {
        digitalWrite(RELAY_2, HIGH);
        client.publish("esp32", "ON2");
        Serial.println("ciaturetubuton2");
      }
      if (RState_2 == LOW) {
        digitalWrite(RELAY_2, LOW);
        client.publish("esp32", "OFF2");
        Serial.println("ciaturetubutoff2");
      }
      Serial.println("Pasikeite_2");
    }
    delayMicroseconds(15);
  }
  LastState_2 = Push_button_state_2;

  if (Push_button_state_3 != LastState_3) {
    if (Push_button_state_3 == LOW) {
      RState_3 = !RState_3;
      if (RState_3 == HIGH) {
        digitalWrite(RELAY_3, HIGH);
        client.publish("esp32", "ON2");
        Serial.println("ciaturetubuton3");
      }
      if (RState_3 == LOW) {
        digitalWrite(RELAY_3, LOW);
        client.publish("esp32", "OFF13");
        Serial.println("ciaturetubutoff3");
      }
      Serial.println("Pasikeite_3");
    }
    delayMicroseconds(15);
  }
  LastState_3 = Push_button_state_3;

  if (Push_button_state_4 != LastState_4) {
    if (Push_button_state_4 == LOW) {
      RState_4 = !RState_4;
      if (RState_4 == HIGH) {
        digitalWrite(RELAY_4, HIGH);
        client.publish("esp32", "ON4");
        Serial.println("ciaturetubuton4");
      }
      if (RState_4 == LOW) {
        digitalWrite(RELAY_4, LOW);
        client.publish("esp32", "OFF4");
        Serial.println("ciaturetubutoff4");
      }
      Serial.println("Pasikeite_4");
    }
    delayMicroseconds(15);
  }
  LastState_4 = Push_button_state_4;

  //sensor
  unsigned long now = millis();
  if (past + 2000 <= now) {
    float humidity = dht.getHumidity();
    float temperature = dht.getTemperature();
    Serial.println(dht.getStatusString());
    Serial.print("\t");
    Serial.print(humidity, 1);
    Serial.print("\t\t");
    client.publish("temp", String(temperature).c_str());
    client.publish("hum", String(humidity).c_str());
    past = now;
  }

}
