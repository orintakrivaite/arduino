/********************  *******************/
#include <Wire.h>
#include <VL53L0X.h>
#include <Servo.h>

Servo myservo;
VL53L0X sensor1;
VL53L0X sensor2;
VL53L0X sensor3;
VL53L0X sensor4;
VL53L0X sensor5;
VL53L0X sensor6;
VL53L0X sensor7;

int laser_pins[] = {2, 3, 4, 5, 6, 7}; // Lazeriu pin
//Motor
int en = 11; // Variklio EN
int in1 = 10; // Atgal
int in2 = 12; // Pirmyn
int servo_pin = 9; // Servo pin
int sensorData[7] = {0};
int servoMin = 50;
int servoMax = 130;
double diff, posn;
int target, pos;

double KP = 0.06; 
double KD = 0.02;
double KI = 0.001;
double sum_diff=0;
double prev_diff=0;


void setup()
{
  Serial.begin (115200);
  laser_setup();
  myservo.attach(servo_pin);
  myservo.write(90);
  pinMode(en, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
}

/******************** Lazeriu setup *******************/
void laser_setup()
{
    for (int i = 0; i < 7; i++) {
    pinMode(laser_pins[i], OUTPUT);
    digitalWrite(laser_pins[i], LOW);
  }
  Wire.begin();


  digitalWrite(2, HIGH);
  sensor1.init(true);
  sensor1.setAddress((uint8_t)01);

  digitalWrite(3, HIGH);
  sensor2.init(true);
  sensor2.setAddress((uint8_t)02);


  digitalWrite(4, HIGH);
  sensor3.init(true);
  sensor3.setAddress((uint8_t)03);


  digitalWrite(5, HIGH);
  sensor4.init(true);
  sensor4.setAddress((uint8_t)04);


  digitalWrite(6, HIGH);
  sensor5.init(true);
  sensor5.setAddress((uint8_t)05);

  digitalWrite(7, HIGH);
  sensor6.init(true);
  sensor6.setAddress((uint8_t)06);

  digitalWrite(8, HIGH);
  sensor7.init(true);
  sensor7.setAddress((uint8_t)07);

  Serial.println("addresses set");

  sensor1.startContinuous();
  sensor2.startContinuous();
  sensor3.startContinuous();
  sensor4.startContinuous();
  sensor5.startContinuous();
  sensor6.startContinuous();
  sensor7.startContinuous();
}
/******************** Lazeriu rezultatai *******************/
void laser()
{
  sensorData[0] = sensor1.readRangeContinuousMillimeters();
  sensorData[1] = sensor2.readRangeContinuousMillimeters();
  sensorData[2] = sensor3.readRangeContinuousMillimeters();
  sensorData[3] = sensor4.readRangeContinuousMillimeters();
  sensorData[4] = sensor5.readRangeContinuousMillimeters();
  sensorData[5] = sensor6.readRangeContinuousMillimeters();
  sensorData[6] = sensor7.readRangeContinuousMillimeters();
}
/******************** Lazeriu rezultatu spausdinimas *******************/
void laser_print()
{
  for (int i = 0; i < 6; i++)
  {
    Serial.print(String(sensorData[i]) + " ");
  }
  Serial.println();
}
/******************** Stabdymo funkcija *******************/
void brakes()
{
  analogWrite(en, 0);
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
}
/*********************Vaziavimas atgal**********************/
void driving_back()
{
  //control speed 
  analogWrite(en, 200);
  //control direction 
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH); 
}
/******************** Važiavimas tiesiai *******************/
void driving_straight()
{
  //control speed 
  analogWrite(en, 90);
  //control direction 
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW); 
}

void loop()
{
    laser();
    laser_print();
    if (sensorData[1] > 1000){
      sensorData[1] = 1000;
    }
    if (sensorData[5] > 1000){
      sensorData[5] = 1000;
    }
    
    diff = sensorData[1]-sensorData[5]; //skirtumas
    target = 90; //servo centro padetis

    posn = target + (diff*KP)+(prev_diff*KD)+(sum_diff*KI);
    pos = constrain(posn, servoMin, servoMax);
    myservo.write(pos);
    Serial.println(posn);
    Serial.println(pos);
    prev_diff=diff; //buvusi paklaida
    sum_diff+=diff; //paklaidu suma 

    if (sensorData[3] < 200){
      driving_back();
      diff = sensorData[5]-sensorData[1]; //skirtumas
      posn = target + (diff*KP)+(prev_diff*KD)+(sum_diff*KI);
      pos = constrain(posn, servoMin, servoMax);
      myservo.write(pos);
      Serial.println(posn);
      Serial.println(pos);
    }
    else if (200 < sensorData[3]){
      //driving_straight();
      //control speed 
      analogWrite(en, 150);
      //control direction 
      digitalWrite(in1, HIGH);
      digitalWrite(in2, LOW);  
      Serial.println("Vaziuoja tiesiai");
    }
}
